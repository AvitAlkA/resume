const path = require("path")
const puppeteer = require("puppeteer")

;(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto(`file:${path.join(__dirname, "index.html")}`)
  await page.pdf({
    path: "AvitalPivenResume.pdf",
    format: "A4",
    printBackground: true,
  })

  await browser.close()
})()
